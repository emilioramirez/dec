#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

from PyQt4.QtGui import QApplication
from PyQt4.QtGui import QMainWindow
from PyQt4.QtGui import QMessageBox
from PyQt4.QtCore import QTranslator
from PyQt4.QtCore import QLibraryInfo
from PyQt4.QtCore import QLocale

from ui.central_widget import CentralWidget


class Window(QMainWindow):

    def __init__(self):
        super(Window, self).__init__()
        self.central = CentralWidget()
        self.setCentralWidget(self.central)
        self.setWindowTitle("DEC")
        # self.showMaximized()
        # self.setMinimumWidth(600)
        # self.setMinimumHeight(600)

    def closeEvent(self, event):
        option = QMessageBox.question(self,
            u"Cerrar aplicación",
            u"¿Está seguro que quiere cerrar la aplicación, perderá todos los datos no guardados.",
            QMessageBox.Yes | QMessageBox.No)
        if option == QMessageBox.Yes:
            super(Window, self).closeEvent(event)
        else:
            event.ignore()
            return

stylesheet = """
QGroupBox {
    border: 1px solid gray;
    border-radius: 5px;
    margin-top: 10px; /* leave space at the top for the title */
}

QGroupBox::title {
    subcontrol-origin: margin;
    subcontrol-position: top center; /* position at the top center */
    padding: 5 10px;
}
"""

app = QApplication(sys.argv)
app.setStyleSheet(stylesheet)

translator = QTranslator()
translator.load("qt_" + QLocale.system().name(),
                  QLibraryInfo.location(QLibraryInfo.TranslationsPath))
app.installTranslator(translator)

w = Window()
w.show()

sys.exit(app.exec_())