#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division
import numpy as np
import codecs
import os
import pickle
from decimal import Decimal
from datetime import datetime
from jinja2 import Environment, FileSystemLoader

# Constantes de la aplicacion
P1 = "p1"  # Ri = Ai / max(Ai)
P3 = "p3"  # Ri = Ai / sum(Ai)
P = (P1, P3)
DATE_FORMAT = "%Y%m%d%H%M%S"


def round(value, round=4):
    """
    """
    PLACES = Decimal(10) ** -(round)
    return float(Decimal(str(value)).quantize(PLACES))


def round_arr(arr, decimals=4):
    return np.around(arr, decimals=decimals)


def normalize_array(arr, method=P3):
    """
    Normaliza matriz y los pesos de acuerdo respecto a la suma:
    Los valores de la matriz arr deben ser floats
    """
    arr_sums = arr.sum(axis=0)
    if method != P3:
        # Sumatoria de las columnas
        arr_sums = arr.max(axis=0)
    # Dividir cada elemento de una columna por su respectiva suma
    arr_norm = arr / arr_sums
    return round_arr(arr_norm)


def save_data_model(data, path="", filename="datos"):
    EXT = "dec"
    timestamp = datetime.now().strftime(DATE_FORMAT)
    name = filename + "-" + data["method"] + "-" + timestamp + "." + EXT
    with open(os.path.join(path, name), "wb") as f:
        pickle.dump(data, f)


def load_data_model(filename=""):
    with open(filename, "rb") as f:
        return pickle.load(f)


class Report(object):

    TEMPLATE_NAME = "report_template.html"
    REPORT_EXTENSION = "html"

    def get_report(self, template_name=TEMPLATE_NAME):
        """
        """
        if not self._resolved:
            print "Es necesario ejecutar resolve antes"
            return
        env = Environment(loader=FileSystemLoader('templates'))
        template = env.get_template(template_name)
        return template.render(self.get_data_report())

    def save_report(self, path="", filename="Reporte"):
        if not self._resolved:
            print "Es necesario ejecutar resolve antes"
            return
        timestamp = datetime.now().strftime(DATE_FORMAT)
        name = filename + "-" + self.__class__.__name__ + "-" + timestamp + "." + self.REPORT_EXTENSION
        with codecs.open(os.path.join(path, name), "wb", 'utf-8') as fh:
            fh.write(self.get_report())
