#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division
import numpy as np
import utils
from datetime import datetime


MAX_CRITERIAS = 7

ERROR_MSGS = {
    "matrix_eq_criteria": "La cantidad de columnas de la matriz no coincide con la cantidad de criterios",
    "max_criterias": "Se recomienda una cantidad de criterios menor o igual a {}.",
    "row_eq_alternatives": "La cantidad de filas de la matriz no coincide con la cantidad de alternativas.",
    "col_eq_weight": "La cantidad de columnas de la matriz no coincide con la cantidad de pesos.",
    "criteria_eq_weight": "La cantidad de criterios no coincide con la cantidad de pesos.",
    "matrix_gt": "Los valores de la matriz de evaluaciones deben ser mayores a {}.",
    "weight_gt": "Los valores de los pesos deben ser positivos mayores a {}.",
    "result_eq_alternatives": "Las cantidad de alterativas y la cantidad de resultados no coinciden",
    "criteria_eq_alternatives_comparison_matrix": "La cantidad de criterios no coincide con la cantidad de matrices de comparacion de alternativas respectivas",
    "matrix_comparison_missing": "No se encuentra la matriz de comparacion de alternativas del criterio {}.",
    "matrix_comparison_of": "La matriz de comparacion de altenativas con respecto al criterio {}. ",
    "not_square": "No es cuadrada",
    "must_be_dim": "Debe ser de dimension {}",
    "matrix_comparison_of_cri_must_be_dim": "La matriz de comparacion de criterios debe ser de dimension {}",
    "matrix_comparison_of_cri_must_be_diag_1": "La matriz de comparacion de criterios tiene diagonal distinta de 1",
    "must_be_diag": "Tiene diagonal distinta de 1",
    "saaty_scale": "Tiene un valor {} fuera del rango {}-{}. Escala Saaty.",
    "matrix_comparison_of_cri_saaty_scale": "La matriz de comparacion de criterios tiene un valor {} fuera del rango {}-{}. Escala Saaty.",
    "inverse_invalid": "Tiene un valor {} inverso invalido.",
    "matrix_comparison_of_cri_inverse_invalid": "La matriz de comparacion de criterios tiene un valor {} inverso invalido.",
    "not_consistent": "No consistente",
    "consistent": "Consistente",
    "consistency_error": "La matriz {} supera el valor de consistencia permitido {}.",
    "alternatives_gt_ri": "La cantidad de alternativas supera la cantidad de indices aleatorios soportados {}",
    "must_resolve_before": "Es necesario ejecutar resolve antes",
}


class LinearPonderation(utils.Report):
    """
    Método de resolución Ponderación Lineal
    """
    NAME = "Ponderación Lineal"
    MIN_VALUE_ALLOWED = 0

    def __init__(self, criteria, alternatives, matrix, weight, **kwargs):
        """
        criteria:
            lista con los criterios str ordenados
            ["IA", "RE", "RI", "TR"]
        alternatives:
            lista con las alternativas str ordenadas
            ["Proyecto 1", "Proyecto 2", "Proyecto 3", "Proyecto 4"]
        matrix:
            Lista de lista con los valores ordenados según alterativas/criterios
            [[8, 14, 1, 0.143],
             [3, 16, 0.2, 0.5],
             [5, 13, 1, 0.25],
             [3, 20, 0.333, 0.2]]
        weight:
            lista con los pesos ordenados según los criterios
            [5, 8, 7, 3]

        """
        self._resolved = False
        self.criteria = criteria
        self.alternatives = alternatives
        # Convertir a un array de floats
        self.matrix = utils.round_arr(np.array(matrix).astype(float))
        self.weight = utils.round_arr(np.array(weight).astype(float))
        self._check_valid_data()

    def _check_valid_data(self):
        """
        Checkea que:
            - la cantidad de columnas de la matriz sea igual a la cantidad de criterios
            - la cantidad de criterios no supere el maximo
            - la cantidad de filas de la matriz sea igual a la cantidad de alternativas
            - la cantidad de columnas de la matriz sea igual a la cantidad de pesos
            - la cantidad de criterios sea igual a la cantidad de pesos
            - los valores de la matriz sean mayores que el minimo permitido
            - los valores de los pesos sean mayores que el minimo permitido
        """
        row, col = self.matrix.shape
        if col != len(self.criteria):
            raise ValueError(ERROR_MSGS["matrix_eq_criteria"])
        if col > MAX_CRITERIAS or len(self.criteria) > MAX_CRITERIAS:
            raise ValueError(ERROR_MSGS["max_criterias"].format(MAX_CRITERIAS))
        if row != len(self.alternatives):
            raise ValueError(ERROR_MSGS["row_eq_alternatives"])
        if col != len(self.weight):
            raise ValueError(ERROR_MSGS["col_eq_weight"])
        if len(self.criteria) != len(self.weight):
            raise ValueError(ERROR_MSGS["criteria_eq_weight"])
        m = self.matrix > LinearPonderation.MIN_VALUE_ALLOWED
        w = self.weight > LinearPonderation.MIN_VALUE_ALLOWED
        if not m.all():
            raise ValueError(ERROR_MSGS["matrix_gt"].format(LinearPonderation.MIN_VALUE_ALLOWED))
        if not w.all():
            raise ValueError(ERROR_MSGS["weight_gt"].format(LinearPonderation.MIN_VALUE_ALLOWED))

    def _get_table(self):
        """
        Crea una tabla (lista de listas) con los datos para el reporte
        """
        index = 0
        matrix = self.matrix.tolist()
        table = []
        table.append([""] + self.criteria)
        for alt in self.alternatives:
            table.append([alt] + matrix[index])
            index += 1
        table.append(["Pesos"] + self.weight.tolist())
        return table

    def _tag_values(self):
        """
        Crea una tupla con las alternativas y su resultado
        """
        if len(self.alternatives) != len(self.result):
            raise ValueError(ERROR_MSGS["result_eq_alternatives"])
        values = []
        for n in range(len(self.alternatives)):
            values.append(
                (self.alternatives[n], utils.round(self.result[n]))
            )
        return values

    def _lineal_ponderation(self, matrix, weight):
        """
        Realiza el producto punto de una matriz con un array
        Ambos deben ser floats
        """
        result = matrix.dot(weight)
        return utils.round_arr(result)

    def resolve(self, normalize=True):
        """
        Resuelve por ponderacion lineal:
            - Si es necesario normaliza la matriz
            - Calcula la ponderaicon lineal
            - Obtiene los resultados etiquetados
            - Ordena los resultados
        """
        if normalize:
            self.matrix_norm = utils.normalize_array(self.matrix)
            self.weight_norm = utils.normalize_array(self.weight)
        else:
            self.matrix_norm = self.matrix
            self.weight_norm = self.weight
        self.result = self._lineal_ponderation(self.matrix_norm, self.weight_norm)
        self.result_taged = self._tag_values()
        self.result_sorted = sorted(self.result_taged, key=lambda tup: tup[1], reverse=True)
        self._resolved = True
        return self.result_sorted

    def get_data_report(self):
        """
        Crea el contexto con datos para renderizar el reporte
        """
        if not self._resolved:
            print ERROR_MSGS["must_resolve_before"]
            return
        context = {}
        context["datetime"] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        context["method"] = self.NAME
        context["criteria"] = self.criteria
        context["alternatives"] = self.alternatives
        context["datum"] = [self._get_table()]
        context["result_sorted"] = self.result_sorted
        return context

    def get_data_model(self):
        """
        Crea el modelo de datos
        """
        data = {
            "method": self.__class__.__name__,
            "criteria": self.criteria,
            "alternatives": self.alternatives,
            "matrix": self.matrix.tolist(),
            "weight": self.weight.tolist()
        }
        return data


class AHP(utils.Report):
    """
    Método de resolución AHP
    """
    NAME = "AHP"

    RI = (0, 0, 0.58, 0.9, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49)
    MIN_VALUE_ALLOWED = 1
    MAX_VALUE_ALLOWED = 9
    CONSISTENCY_UMBRAL = 0.1

    def __init__(self, criteria, alternatives, matrix_comparison_alternatives,
                 matrix_comparison_criteria, **kwargs):
        """
        criteria:
            lista con los criterios str ordenados
            ["P", "VP", "G", "ST"]
        alternatives:
            lista con las alternativas str ordenadas
            ["HP", "IBM", "COMPAQ"]
        matrix_comparison_alternatives:
            Diccionario con las matrices de comparaciones pareadas
            {
                "P": [[1, 2, 3],
                      [1./2, 1, 2],
                      [1./3, 1./2, 1]],
                "VP": [[1, 1./2, 1./4],
                      [2, 1, 1./3],
                      [4, 3, 1]],
                "G": [[1, 1./6, 1./3],
                      [6, 1, 3],
                      [3, 1./3, 1]],
                "ST": [[1, 1./4, 1./7],
                      [4, 1, 1],
                      [7, 1, 1]],
            }
        matrix_comparison_criteria:
            Lista de lista con los valores de comparación pareadas de las alternativas
            [[1, 3, 2, 5],
             [1./3, 1, 1./2, 1./2],
             [1./2, 2, 1, 2],
             [1./5, 2, 1./2, 1],]
        """
        self._resolved = False
        self.criteria = criteria
        self.alternatives = alternatives
        self.matrix_comparison_alternatives = {}
        for criteria, matrix in matrix_comparison_alternatives.items():
            # Convertir a un array de floats
            self.matrix_comparison_alternatives[criteria] = utils.round_arr(np.array(matrix).astype(float))
            self._check_valid_data(criteria, self.matrix_comparison_alternatives[criteria],
                                   len(self.alternatives))

        self.matrix_comparison_criteria = utils.round_arr(np.array(matrix_comparison_criteria).astype(float))
        self._check_required_data()
        self._check_valid_data("criteria_comparison", self.matrix_comparison_criteria,
                               len(self.criteria))
        self._calculate_all_consistency()

    def _check_required_data(self):
        """
        Checkea que:
            - por cada criterio exista una matriz de comparación
            - la cantidad de criterios no supere el maximo recomendado
            - la cantidad de criterios coincida con la cantidad de matrices
            - la cantidad de alternativas no supere el maximo dado por la
                cantidad de valores aleatorios que se tienen
        """
        if len(self.criteria) > MAX_CRITERIAS:
            raise ValueError(ERROR_MSGS["max_criterias"].format(MAX_CRITERIAS))
        if len(self.alternatives) > len(AHP.RI):
            raise ValueError(ERROR_MSGS["alternatives_gt_ri"].format(len(AHP.RI)))
        if len(self.criteria) != len(self.matrix_comparison_alternatives.keys()):
            raise ValueError(ERROR_MSGS["criteria_eq_alternatives_comparison_matrix"])
        for c in self.criteria:
            if c not in self.matrix_comparison_alternatives.keys():
                raise ValueError(ERROR_MSGS["matrix_comparison_missing"].format(c))

    def _check_valid_data(self, name, matrix, length):
        """
        Checkea que:
            - la matriz sea cuadrada
            - la dimensión de la matriz coincida con length
            - la diagonal es 1
            - los enteros esten en rango 1-9
            - los inversos esten entre 0-1
        """
        error = ERROR_MSGS["matrix_comparison_of"].format(name)
        row, col = matrix.shape
        if col != row:
            raise ValueError(error + ERROR_MSGS["not_square"])
        if col != length:
            if name == "criteria_comparison":
                raise ValueError(ERROR_MSGS["matrix_comparison_of_cri_must_be_dim"].format(length))
            raise ValueError(error + ERROR_MSGS["must_be_dim"].format(length))
        for diag in matrix.diagonal():
            if diag != 1:
                if name == "criteria_comparison":
                    raise ValueError(ERROR_MSGS["matrix_comparison_of_cri_must_be_diag_1"])
                raise ValueError(error + ERROR_MSGS["must_be_diag"])
        for i in range(row):
            for j in range(col):
                if matrix[i][j].is_integer():
                    if matrix[i][j] < AHP.MIN_VALUE_ALLOWED or matrix[i][j] > AHP.MAX_VALUE_ALLOWED:
                        if name == "criteria_comparison":
                            raise ValueError(ERROR_MSGS["matrix_comparison_of_cri_saaty_scale"].format(matrix[i][j], AHP.MIN_VALUE_ALLOWED, AHP.MAX_VALUE_ALLOWED))
                        raise ValueError(error + ERROR_MSGS["saaty_scale"].format(matrix[i][j], AHP.MIN_VALUE_ALLOWED, AHP.MAX_VALUE_ALLOWED))
                else:
                    if matrix[i][j] <= 0 or matrix[i][j] >= 1:
                        if name == "criteria_comparison":
                            raise ValueError(ERROR_MSGS["matrix_comparison_of_cri_inverse_invalid"].format(matrix[i][j]))
                        raise ValueError(error + ERROR_MSGS["inverse_invalid"].format(matrix[i][j]))

    def _calculate_weight(self, arr):
        """
        Obtiene los pesos dada las matriz de comparacion binaria segun
        metodo AHP.
        """
        # Obtenemos los autovalores y los autovectores
        autovalores, autovectores = np.linalg.eig(arr)

        # Obtenemos el mayor autovalor
        autovalor = max(autovalores)
        autovalor_idx = autovalores.tolist().index(autovalor)
        # Obtenemos su autovector
        autovector = autovectores.take((autovalor_idx,), axis=1)

        # Normalizamos el autovector
        normalized = utils.normalize_array(autovector)

        # Convertimos en una lista de nros reales
        vector = [abs(e[0]) for e in normalized]

        return utils.round_arr(vector)

    def _calculate_consistency(self, arr):
        """
        Obtiene el peso de la matriz de comparacion binaria y calcula el
        ratio de consistencia.
        """
        # Obtenemos los autovalores y los autovectores
        autovalores, autovectores = np.linalg.eig(arr)
        autovalor_max = max(autovalores).real

        # Indice de consistencia
        n = len(arr)
        IC = (autovalor_max - n) / (n - 1)

        # Ratio de consistencia
        RC = IC / float(AHP.RI[n])
        return utils.round(RC)

    def _calculate_all_consistency(self):
        """
        Calcula la consistencia de todas las matrices
        """
        self.alternatives_consistency = {}

        for criteria, matrix in self.matrix_comparison_alternatives.items():
            matrix_consistency_value = self._calculate_consistency(matrix)
            self._check_consistency(criteria, matrix_consistency_value)
            self.alternatives_consistency[criteria] = (matrix_consistency_value, ERROR_MSGS["consistent"])

        matrix_consistency_value = self._calculate_consistency(self.matrix_comparison_criteria)
        self._check_consistency("Comparacion binaria de criterios", matrix_consistency_value)
        self.criteria_consistency = (matrix_consistency_value, ERROR_MSGS["consistent"])

    def _check_consistency(self, matrix_name, value):
        """
        Detecta si alguna matriz es inconsistente
        """
        if value > AHP.CONSISTENCY_UMBRAL:
            raise ValueError(ERROR_MSGS["consistency_error"].format(matrix_name, value))

    def _calculate_matrix_norm(self):
        """
        Obtiene la matriz normalizada usando todos las matrices de
        comparaciones binarias entre alternativas.
        """
        values = []
        for criteria in self.criteria:
            values.append(self._calculate_weight(self.matrix_comparison_alternatives[criteria]))
        return np.vstack(values).T

    def _calculate_weight_norm(self):
        """
        Obtiene el vector de pesos usando la matriz de comparaciones binarias entre criterios
        """
        return self._calculate_weight(self.matrix_comparison_criteria)

    def resolve(self):
        """
        Resuelve por AHP:
            - Obtiene la matriz normalizada
            - Obtiene los pesos normalizados
            - Calcula por Ponderacion lineal el resultado
        """
        self.matrix_norm = self._calculate_matrix_norm()
        self.weight_norm = self._calculate_weight_norm()
        lp = LinearPonderation(self.criteria, self.alternatives, self.matrix_norm,
                               self.weight_norm)
        lp.resolve(normalize=False)
        self.result = lp.result
        self.result_taged = lp.result_taged
        self.result_sorted = lp.result_sorted
        self._resolved = True
        return self.result_sorted

    def get_data_report(self):
        """
        Crea el contexto con datos para renderizar el reporte
        """
        if not self._resolved:
            print ERROR_MSGS["must_resolve_before"]
            return
        context = {}
        context["datetime"] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        context["method"] = self.NAME
        context["criteria"] = self.criteria
        context["alternatives"] = self.alternatives
        context["datum"] = self._get_tables()
        context["consistencies"] = self._get_consistency()
        context["result_sorted"] = self.result_sorted
        return context

    def _get_tables(self):
        """
        Crea una lista de tablas (lista de listas) con los datos para el reporte
        """
        table = []
        for cri in self.criteria:
            table.append(self._get_table_alternative(cri))
        table.append(self._get_table_criteria())
        return table

    def _get_table_alternative(self, criteria):
        matrix = self.matrix_comparison_alternatives[criteria].tolist()
        table = []
        table.append([criteria] + self.alternatives)
        index = 0
        for alt in self.alternatives:
            table.append([alt] + matrix[index])
            index += 1
        return table

    def _get_table_criteria(self):
        matrix = self.matrix_comparison_criteria.tolist()
        table = []
        table.append([""] + self.criteria)
        index = 0
        for cri in self.criteria:
            table.append([cri] + matrix[index])
            index += 1
        return table

    def _get_consistency(self):
        consistency = self.alternatives_consistency
        consistency["criterios"] = self.criteria_consistency
        return consistency

    def get_data_model(self):
        """
        Crea el modelo de datos
        """
        data = {
            "method": self.__class__.__name__,
            "criteria": self.criteria,
            "alternatives": self.alternatives,
            "matrix_comparison_alternatives": self.matrix_comparison_alternatives,
            "matrix_comparison_criteria": self.matrix_comparison_criteria.tolist()
        }
        return data


class Topsis(LinearPonderation):
    """
    Método de resolución TOPSIS
    """
    NAME = "Topsis"

    def resolve(self):
        """
        """
        self.matrix_norm = utils.normalize_array(self.matrix)
        self.weight_norm = utils.normalize_array(self.weight)

        # Normalized and ponderaded
        self.matrix_weighted = self._calculate_matrix_values(self.matrix_norm, self.weight_norm)

        # A+
        self.matrix_A_plus = self.matrix_weighted.max(axis=0)
        # A-
        self.matrix_A_minus = self.matrix_weighted.min(axis=0)

        # S+
        matrix_S_plus = self.matrix_A_plus - self.matrix_weighted
        s_plus = matrix_S_plus.sum(axis=1)

        # S-
        matrix_S_minus = self.matrix_weighted - self.matrix_A_minus
        s_minus = matrix_S_minus.sum(axis=1)

        self.result = self._calculate_similarity(s_plus, s_minus)
        self.result_taged = self._tag_values()
        self.result_sorted = sorted(self.result_taged, key=lambda tup: tup[1], reverse=True)
        self._resolved = True
        return self.result_sorted

    def _calculate_similarity(self, s_plus, s_minus):
        """
        Para cada alternativa se calcula su indice de similaridad
        """
        C = []
        for alt in self.alternatives:
            i = self.alternatives.index(alt)
            C.append((s_minus[i]) / (s_plus[i] + s_minus[i]))
        return C

    def _calculate_matrix_values(self, matrix_norm, weight_norm):
        """
        Calcula la matriz V
        """
        return utils.round_arr(np.multiply(matrix_norm, weight_norm))
