#!/usr/bin/python
# -*- coding: utf-8 -*-
from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QVBoxLayout
from PyQt4.QtGui import QHBoxLayout
from PyQt4.QtGui import QFormLayout
from PyQt4.QtGui import QLineEdit
from PyQt4.QtGui import QPushButton
from PyQt4.QtCore import SIGNAL


class ConfigureDialog(QDialog):

    def __init__(self, criterios, alternativas):
        super(ConfigureDialog, self).__init__()

        vbox = QVBoxLayout(self)
        self.form = QFormLayout()
        vbox.addLayout(self.form)
        self._criteria = criterios
        self._alternative = alternativas
        self.change_names = False

        for item in range(criterios):
            index = item + 1
            self.form.addRow(
                "Criterio %s:" % index, QLineEdit("Criterio %s" % index))
        for item in range(alternativas):
            index = item + 1
            self.form.addRow(
                "Alternativa %s:" % index, QLineEdit("Alternativa %s" % index))

        btn_accept = QPushButton("Aceptar")
        btn_cancel = QPushButton("Cancelar")
        hbox = QHBoxLayout()
        hbox.addWidget(btn_cancel)
        hbox.addWidget(btn_accept)
        vbox.addLayout(hbox)

        self.connect(btn_cancel, SIGNAL("clicked()"), self.close)
        self.connect(btn_accept, SIGNAL("clicked()"), self._accept)

    def _accept(self):
        self.change_names = True
        self.close()

    def get_configuration(self):
        names = []
        for index in range(self.form.rowCount()):
            item = self.form.itemAt(index, QFormLayout.FieldRole)
            widget = item.widget()
            names.append(widget.text())

        return names

    def set_configuration(self, h_headers, v_headers):
        names = h_headers + v_headers
        for index in range(self.form.rowCount()):
            item = self.form.itemAt(index, QFormLayout.FieldRole)
            widget = item.widget()
            widget.setText(names[index])
