#!/usr/bin/python
# -*- coding: utf-8 -*-
from PyQt4.QtGui import QWidget
from PyQt4.QtGui import QVBoxLayout
from PyQt4.QtGui import QHBoxLayout
from PyQt4.QtGui import QGridLayout
from PyQt4.QtGui import QStackedLayout
from PyQt4.QtGui import QSpinBox
from PyQt4.QtGui import QSlider
from PyQt4.QtGui import QPushButton
from PyQt4.QtGui import QLabel
from PyQt4.QtGui import QComboBox
from PyQt4.QtGui import QSpacerItem
from PyQt4.QtGui import QSizePolicy
from PyQt4.QtGui import QGroupBox
from PyQt4.QtGui import QFileDialog
from PyQt4.QtGui import QMessageBox
from PyQt4.QtCore import Qt
from PyQt4.QtCore import QDir
from PyQt4.QtCore import SIGNAL

from single_table import SingleTable
from multiple_tables import MultipleTables
from configure_dialog import ConfigureDialog
from results import Results

import core
import utils
import os


HOME_PATH = unicode(QDir.toNativeSeparators(QDir.homePath()))

METODOS = [
    "TOPSIS",
    "AHP",  # index = 1: multiple tables
    "PONDERACION LINEAL"
]


class CentralWidget(QWidget):

    def __init__(self):
        super(CentralWidget, self).__init__()
        self.__max_range_alternative = len(core.AHP.RI)
        self.__max_range_criteria = core.MAX_CRITERIAS
        self.__min_range = 2
        self._current = None

        vbox = QVBoxLayout(self)
        groupHeader = QGroupBox()
        groupHeader.setTitle(u"Parámetros")
        vboxHeader = QVBoxLayout(groupHeader)
        # Spinners
        self._spin_alternativas = QSpinBox()
        self._spin_alternativas.setMinimum(self.__min_range)
        self._spin_alternativas.setMaximum(self.__max_range_alternative)
        self._spin_criterios = QSpinBox()
        self._spin_criterios.setMinimum(self.__min_range)
        self._spin_criterios.setMaximum(self.__max_range_criteria)
        # Sliders
        self._slide_alternativas = QSlider(Qt.Horizontal)
        self._slide_alternativas.setMinimum(self.__min_range)
        self._slide_alternativas.setMaximum(self.__max_range_alternative)
        self._slide_criterios = QSlider(Qt.Horizontal)
        self._slide_criterios.setMinimum(self.__min_range)
        self._slide_criterios.setMaximum(self.__max_range_criteria)
        # Combo
        self._combo_metodo = QComboBox()
        for metodo in METODOS:
            self._combo_metodo.addItem(metodo)

        grid = QGridLayout()
        grid.addWidget(QLabel("Alternativas:"), 0, 0)
        grid.addWidget(self._slide_alternativas, 0, 1)
        grid.addWidget(self._spin_alternativas, 0, 2)
        grid.addWidget(QLabel("Criterios:"), 1, 0)
        grid.addWidget(self._slide_criterios, 1, 1)
        grid.addWidget(self._spin_criterios, 1, 2)
        grid.addWidget(QLabel(u"Método:"), 2, 0)
        grid.addWidget(self._combo_metodo, 2, 1)
        vboxHeader.addLayout(grid)

        self._btn_limpiar = QPushButton("Limpiar datos")
        self._btn_cargar = QPushButton("Cargar modelo")
        self._btn_resolver = QPushButton("Resolver")
        self._btn_guardar = QPushButton("Guardar modelo")
        self._btn_configurar = QPushButton("Configurar Nombres")
        hbox = QHBoxLayout()
        hbox.addWidget(self._btn_resolver)
        hbox.addSpacerItem(QSpacerItem(1, 0, QSizePolicy.Expanding))
        hbox.addWidget(self._btn_limpiar)
        hbox.addWidget(self._btn_cargar)
        hbox.addWidget(self._btn_guardar)
        vboxHeader.addLayout(hbox)

        hbox_2 = QHBoxLayout()
        hbox_2.addWidget(self._btn_configurar)
        hbox_2.addSpacerItem(QSpacerItem(1, 0, QSizePolicy.Expanding))

        vbox.addWidget(groupHeader)

        vbox.addLayout(hbox_2)

        self.connect(self._spin_alternativas, SIGNAL("valueChanged(int)"),
                     self._slide_alternativas.setValue)
        self.connect(self._spin_alternativas, SIGNAL("valueChanged(int)"),
                     self.set_rows)
        self.connect(self._spin_criterios, SIGNAL("valueChanged(int)"),
                     self._slide_criterios.setValue)
        self.connect(self._spin_criterios, SIGNAL("valueChanged(int)"),
                     self.set_cols)
        self.connect(self._slide_alternativas, SIGNAL("valueChanged(int)"),
                     self._spin_alternativas.setValue)
        self.connect(self._slide_criterios, SIGNAL("valueChanged(int)"),
                     self._spin_criterios.setValue)
        self.connect(self._combo_metodo, SIGNAL("currentIndexChanged(int)"),
                     self.set_current)
        self.connect(self._btn_limpiar, SIGNAL("clicked()"),
                     self.clean)
        self.connect(self._btn_configurar, SIGNAL("clicked()"),
                     self.configure)
        self.connect(self._btn_resolver, SIGNAL("clicked()"),
                     self.resolve)
        self.connect(self._btn_guardar, SIGNAL("clicked()"),
                     self.save_data_model)
        self.connect(self._btn_cargar, SIGNAL("clicked()"),
                     self.load_data_model)

        # Tables Area
        self._stacked = QStackedLayout()
        self._single = SingleTable()
        self._multiple = MultipleTables()
        self._stacked.addWidget(self._single)
        self._stacked.addWidget(self._multiple)

        vbox.addLayout(self._stacked)

        self.set_current(0)
        self._multiple.set_table_names()
        self._single.set_table_names()

    def resolve(self):
        method_object = self._return_method_object()
        if method_object:
            results = method_object.resolve()
            # Crear los dialogos de informe
            res = Results(method_object)
            res.set_model(results)
            res.exec_()

    def _return_method_object(self):
        if self._current is self._single:
            criteria_names, alternatives_names, matrix, weights = \
                self._single.resolve()
            try:
                method = self._combo_metodo.currentIndex()
                if method == 0:
                    method_object = core.Topsis(criteria_names, alternatives_names, matrix, weights)
                else:
                    method_object = core.LinearPonderation(criteria_names, alternatives_names, matrix, weights)
                return method_object
            except ValueError as error:
                QMessageBox.critical(self, "Ha ocurrido un error", error.message)
        else:
            criteria_names, alternatives_names, matrix, criteria_data = \
                self._multiple.resolve()
            try:
                ahp = core.AHP(criteria_names, alternatives_names, matrix, criteria_data)
                return ahp
            except ValueError as error:
                QMessageBox.critical(self, "Ha ocurrido un error", error.message)

    def save_data_model(self):
        method_object = self._return_method_object()
        if method_object:
            data = method_object.get_data_model()
            filename = QFileDialog.getSaveFileName(self,
                                                   "Guardar modelo de datos",
                                                   os.path.join(HOME_PATH, "datos"),
                                                   "DEC (*.dec)")
            if filename:
                path, name = os.path.split(unicode(filename))
                utils.save_data_model(data, path=path, filename=name)

    def load_data_model(self):
        filename = QFileDialog.getOpenFileName(self,
                                               "Cargar modelo de datos",
                                               os.path.join(HOME_PATH, "datos"),
                                               "DEC (*.dec)")
        if filename:
            data = utils.load_data_model(unicode(filename))
            if data["method"] in (
                    core.Topsis.__name__, core.LinearPonderation.__name__):
                self.set_current(0)
                if data["method"] == core.LinearPonderation.__name__:
                    self._combo_metodo.setCurrentIndex(2)
                else:
                    self._combo_metodo.setCurrentIndex(0)
            else:
                self.set_current(1)
                self._combo_metodo.setCurrentIndex(1)
            criteria = data["criteria"]
            alternatives = data["alternatives"]
            self._slide_alternativas.setValue(len(alternatives))
            self._slide_criterios.setValue(len(criteria))
            self._current.set_cols(len(criteria))
            self._current.set_rows(len(alternatives))
            self._current.set_names(criteria, alternatives)
            self._current.load_data(data)

    def clean(self):
        option = QMessageBox.question(self,
            u"Limpiar los datos del modelo",
            u"¿Está seguro que quiere limpiar los datos del modelo, perderá todos los datos no guardados.",
            QMessageBox.Yes | QMessageBox.No)
        if option == QMessageBox.Yes:
            self._slide_alternativas.setValue(2)
            self._slide_criterios.setValue(2)
            self._multiple.clean()
            self._single.clean()

    def configure(self):
        c = self._slide_criterios.value()
        a = self._slide_alternativas.value()
        h_header, v_header = self._current.get_headers()
        dialog = ConfigureDialog(c, a)
        dialog.set_configuration(h_header, v_header)
        dialog.exec_()
        if dialog.change_names:
            names = dialog.get_configuration()
            self._single.set_names(names[:c], names[c:])
            self._multiple.set_names(names[:c], names[c:])

    def set_current(self, index):
        if index == 1:
            widget = self._multiple
        else:
            widget = self._single
        self._stacked.setCurrentWidget(widget)
        self._current = widget

        rows = self._spin_alternativas.value()
        cols = self._spin_criterios.value()
        self._current.set_cols(cols)
        self._current.set_rows(rows)

    def set_rows(self, value):
        self._current.set_rows(value)

    def set_cols(self, value):
        self._current.set_cols(value)
        if self._current is self._multiple:
            self._current.set_rows(self._slide_alternativas.value())
