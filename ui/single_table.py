#!/usr/bin/python
# -*- coding: utf-8 -*-
from PyQt4.QtGui import QWidget
from PyQt4.QtGui import QVBoxLayout
from PyQt4.QtGui import QTableWidget
from PyQt4.QtGui import QTableWidgetItem
from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import Qt

import utils


class SingleTable(QWidget):

    def __init__(self):
        super(SingleTable, self).__init__()
        self._currentVerticalHeaders = []
        self._currentHorizontalHeaders = []

        vbox = QVBoxLayout(self)
        self._main_table = QTableWidget()
        self._main_table.setAlternatingRowColors(True)
        self._weight_table = QTableWidget()
        self._weight_table.setFixedHeight(50)
        self._weight_table.horizontalHeader().hide()

        vbox.addWidget(self._main_table)
        vbox.addWidget(self._weight_table)
        self._weight_table.setRowCount(1)
        self._weight_table.setVerticalHeaderLabels(["Pesos"])

        self.connect(self._main_table, SIGNAL("cellChanged(int, int)"),
                     self._check_value)
        self.connect(self._weight_table, SIGNAL("cellChanged(int, int)"),
                     self._check_value)

    def clean(self):
        rows = self._main_table.rowCount()
        cols = self._main_table.columnCount()
        for row in range(rows):
            for col in range(cols):
                item = self._main_table.item(row, col)
                if item:
                    item.setText("0.0")
        self.set_table_names()

    def get_headers(self):
        rows = self._main_table.rowCount()
        cols = self._main_table.columnCount()
        alternatives_names = []
        criteria_names = []
        for index in range(rows):
            alternative = self._main_table.verticalHeaderItem(index)
            alternatives_names.append(unicode(alternative.text()))
        for index in range(cols):
            criteria = self._main_table.horizontalHeaderItem(index)
            criteria_names.append(unicode(criteria.text()))
        return criteria_names, alternatives_names

    def resolve(self):
        alternatives_names = []
        criteria_names = []
        criteria_names, alternatives_names = self.get_headers()

        matrix = []
        rows = self._main_table.rowCount()
        cols = self._main_table.columnCount()
        for row in range(rows):
            row_data = []
            for col in range(cols):
                item = self._main_table.item(row, col)
                if not item:
                    item = QTableWidgetItem("0.0")
                row_data.append(float(item.text()))
            matrix.append(row_data)

        weights = []
        cols = self._weight_table.columnCount()
        for col in range(cols):
            item = self._weight_table.item(0, col)
            if not item:
                item = QTableWidgetItem("0.0")
            weights.append(float(item.text()))

        return criteria_names, alternatives_names, matrix, weights

    def set_table_names(self):
        rows = self._main_table.rowCount()
        cols = self._main_table.columnCount()
        row_names = []
        col_names = []
        for col in range(cols):
            if col < len(self._currentHorizontalHeaders):
                col_names.append(self._currentHorizontalHeaders[col])
            else:
                col_names.append("Criterio {}".format(col + 1))
        for row in range(rows):
            if row < len(self._currentVerticalHeaders):
                row_names.append(self._currentVerticalHeaders[row])
            else:
                row_names.append("Alternativa {}".format(row + 1))

        self._main_table.setHorizontalHeaderLabels(col_names)
        self._main_table.setVerticalHeaderLabels(row_names)

    def set_names(self, horizontal, vertical):
        self._main_table.setHorizontalHeaderLabels(horizontal)
        self._main_table.setVerticalHeaderLabels(vertical)

    def _check_value(self, row, col):
        table = self.sender()
        item = table.item(row, col)
        item_val = str(item.text())
        try:
            value = float(item_val)
            item.setText(str(utils.round(value)))
        except ValueError as error:
            item.setText("0.0")

    def set_rows(self, rows):
        self._currentHorizontalHeaders, self._currentVerticalHeaders = \
            self.get_headers()
        self._main_table.setRowCount(rows)
        self.set_table_names()

    def set_cols(self, cols):
        self._currentHorizontalHeaders, self._currentVerticalHeaders = \
            self.get_headers()
        self._main_table.setColumnCount(cols)
        self._weight_table.setColumnCount(cols)
        self.set_table_names()

    def load_data(self, data):
        weights = data["weight"]
        matrix = data["matrix"]
        rows = self._main_table.rowCount()
        cols = self._main_table.columnCount()
        for row in range(rows):
            for col in range(cols):
                item = self._main_table.item(row, col)
                if not item:
                    item = QTableWidgetItem(str(matrix[row][col]))
                    item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled |
                                  Qt.ItemIsEditable)
                    self._main_table.setItem(row, col, item)
                item.setText(str(matrix[row][col]))

        for col in range(cols):
            item = self._weight_table.item(0, col)
            if not item:
                item = QTableWidgetItem(str(weights[col]))
                item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled |
                              Qt.ItemIsEditable)
                self._weight_table.setItem(0, col, item)
            item.setText(str(weights[col]))
