# -*- coding: utf-8 -*-
import os
import random

from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QVBoxLayout
from PyQt4.QtGui import QHBoxLayout
from PyQt4.QtGui import QPushButton
from PyQt4.QtGui import QColor
from PyQt4.QtGui import QSpacerItem
from PyQt4.QtGui import QSizePolicy
from PyQt4.QtGui import QFileDialog
from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import QUrl
from PyQt4.QtCore import QDir
from PyQt4.QtDeclarative import QDeclarativeView


HOME_PATH = unicode(QDir.toNativeSeparators(QDir.homePath()))


class Results(QDialog):

    def __init__(self, method_object, parent=None):
        super(Results, self).__init__(parent)
        self.method_object = method_object
        self.setMinimumWidth(600)
        self.setMinimumHeight(600)
        self.setWindowTitle(u"Resultados del Análisis")
        vbox = QVBoxLayout(self)
        self.view = QDeclarativeView()
        self.view.setMinimumWidth(400)
        self.view.setResizeMode(QDeclarativeView.SizeRootObjectToView)
        path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(path, "Results.qml")
        self.view.setSource(QUrl(path))
        self.root = self.view.rootObject()
        vbox.addWidget(self.view)

        btn = QPushButton("Guardar reporte")
        hbox = QHBoxLayout()
        hbox.addSpacerItem(QSpacerItem(1, 0, QSizePolicy.Expanding))
        hbox.addWidget(btn)
        vbox.addLayout(hbox)

        self.connect(btn, SIGNAL("clicked()"), self.save_report)

    def save_report(self):
        if self.method_object:
            filename = QFileDialog.getSaveFileName(self,
                                                   "Guardar resultados",
                                                   os.path.join(HOME_PATH, "resultados"),
                                                   "HTML (*.html)")
            if filename:
                path, name = os.path.split(unicode(filename))
                self.method_object.save_report(path=path, filename=name)

    def _get_random_color(self, mix=None):
        red = random.randint(0, 256)
        green = random.randint(0, 256)
        blue = random.randint(0, 256)

        # mix the color
        if mix:
            red = (red + mix.red()) / 2
            green = (green + mix.green()) / 2
            blue = (blue + mix.blue()) / 2

        color = QColor(red, green, blue)
        return color.name()

    def set_model(self, model=None):
        self.root.set_title(u"Resultados del Análisis")
        self.root.set_subtitle(u"Se sugiere elegir la alternativa: {}".format(model[0][0]))
        results = []
        for name, number in model:
            results.append([name, self._get_random_color(), number])
        self.root.set_model(results)
