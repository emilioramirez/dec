#!/usr/bin/python
# -*- coding: utf-8 -*-
from PyQt4.QtGui import QWidget
from PyQt4.QtGui import QTabWidget
from PyQt4.QtGui import QVBoxLayout
from PyQt4.QtGui import QTableWidget
from PyQt4.QtGui import QTableWidgetItem
from PyQt4.QtCore import Qt
from PyQt4.QtCore import SIGNAL

import utils


class MultipleTables(QWidget):

    def __init__(self):
        super(MultipleTables, self).__init__()
        self._currentVerticalHeaders = []
        self._currentHorizontalHeaders = []
        vbox = QVBoxLayout(self)
        self._tab_widget = QTabWidget()
        self._main_table = QTableWidget()
        self._main_table.setAlternatingRowColors(True)
        vbox.addWidget(self._tab_widget)
        self._ignore_signal = False

        self._tab_widget.addTab(self._main_table, "Comparacion de Criterios")

        self.connect(self._main_table, SIGNAL("cellChanged(int, int)"),
                     self.modify_proportional)

    def get_headers(self):
        alternatives_names = []
        criteria_names = []
        if self._tab_widget.count() < 2:
            return [], []
        widget = self._tab_widget.widget(0)
        rows = widget.rowCount()
        for index in range(rows):
            criteria = widget.horizontalHeaderItem(index)
            criteria_names.append(unicode(criteria.text()))
        widget = self._tab_widget.widget(1)
        rows = widget.rowCount()
        for index in range(rows):
            alternative = widget.verticalHeaderItem(index)
            alternatives_names.append(unicode(alternative.text()))

        return criteria_names, alternatives_names

    def resolve(self):
        criteria_names, alternatives_names = self.get_headers()

        criteria_data = []
        key = unicode(self._tab_widget.tabText(0))
        widget = self._tab_widget.widget(0)
        rows = widget.rowCount()
        cols = widget.columnCount()
        for row in range(rows):
            row_data = []
            for col in range(cols):
                item = widget.item(row, col)
                if not item:
                    item = QTableWidgetItem("0.0")
                row_data.append(float(item.text()))
            criteria_data.append(row_data)

        matrix = {}
        for index in range(1, self._tab_widget.count()):
            key = unicode(self._tab_widget.tabText(index))
            widget = self._tab_widget.widget(index)
            rows = widget.rowCount()
            cols = widget.columnCount()
            table_data = []
            for row in range(rows):
                row_data = []
                for col in range(cols):
                    item = widget.item(row, col)
                    if not item:
                        item = QTableWidgetItem("0.0")
                    row_data.append(float(item.text()))
                table_data.append(row_data)
            matrix[key] = table_data

        return criteria_names, alternatives_names, matrix, criteria_data

    def set_names(self, horizontal, vertical):
        widget = self._tab_widget.widget(0)
        widget.setHorizontalHeaderLabels(horizontal)
        widget.setVerticalHeaderLabels(horizontal)

        for index in range(1, self._tab_widget.count()):
            widget = self._tab_widget.widget(index)
            self._tab_widget.setTabText(index, horizontal[index - 1])
            widget.setHorizontalHeaderLabels(vertical)
            widget.setVerticalHeaderLabels(vertical)

    def clean(self):
        for index in range(self._tab_widget.count()):
            widget = self._tab_widget.widget(index)
            rows = widget.rowCount()
            cols = widget.columnCount()
            for row in range(rows):
                for col in range(cols):
                    item = widget.item(row, col)
                    if not item:
                        item = QTableWidgetItem("0.0")
                        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled |
                                      Qt.ItemIsEditable)
                        widget.setItem(row, col, item)
                    item.setText("0.0")

    def set_table_names(self):
        for index in range(self._tab_widget.count()):
            names = []
            widget = self._tab_widget.widget(index)
            rows = widget.rowCount()
            for row in range(rows):
                word = "Alternativa " + str(row + 1)
                if row < len(self._currentVerticalHeaders):
                    word = self._currentVerticalHeaders[row]
                if index == 0:
                    word = "Criterio " + str(row + 1)
                    if row < len(self._currentHorizontalHeaders):
                        word = self._currentHorizontalHeaders[row]
                names.append(word)
            widget.setHorizontalHeaderLabels(names)
            widget.setVerticalHeaderLabels(names)

    def set_cols(self, cols):
        self._currentHorizontalHeaders, self._currentVerticalHeaders = \
            self.get_headers()
        self._main_table.setRowCount(cols)
        self._main_table.setColumnCount(cols)

        for index in range(cols):
            item = QTableWidgetItem("1")
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self._main_table.setItem(index, index, item)

        tabs_count = self._tab_widget.count() - 1
        if cols < tabs_count:
            tabs_count -= cols
            for tab in range(tabs_count):
                index = self._tab_widget.count() - 1
                widget = self._tab_widget.widget(index)
                self._tab_widget.removeTab(index)
                del widget
        else:
            to_add = cols - tabs_count
            for col in range(to_add):
                self.create_table()

        self.set_table_names()

    def set_rows(self, rows):
        tabs_count = self._tab_widget.count()
        for tab in range(1, tabs_count):
            self.set_alternativas_rows(tab, rows)
        self.set_table_names()

    def create_table(self):
        table = QTableWidget()
        table.setAlternatingRowColors(True)
        self._tab_widget.addTab(
            table, "Criterio %d" % self._tab_widget.count())

        self.connect(table, SIGNAL("cellChanged(int, int)"),
                     self.modify_proportional)

    def set_alternativas_rows(self, index, rows):
        table = self._tab_widget.widget(index)
        table.setRowCount(rows)
        table.setColumnCount(rows)

        for index in range(rows):
            item = QTableWidgetItem("1")
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            table.setItem(index, index, item)

    def modify_proportional(self, row, col):
        if row != col and not self._ignore_signal:
            self._ignore_signal = True
            table = self.sender()
            item = table.item(row, col)
            item_val = str(item.text())
            try:
                item_val = float(item_val)
                value = str(utils.round(1.0 / item_val))
                newitem = QTableWidgetItem(value)
                table.setItem(col, row, newitem)
            except ValueError as error:
                item.setText("0.0")
        else:
            self._ignore_signal = False

    def load_data(self, data):

        criteria_data = data["matrix_comparison_criteria"]
        widget = self._tab_widget.widget(0)
        rows = widget.rowCount()
        cols = widget.columnCount()
        for row in range(rows):
            for col in range(cols):
                self._ignore_signal = True
                item = widget.item(row, col)
                if not item:
                    item = QTableWidgetItem(str(criteria_data[row][col]))
                    item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled |
                                  Qt.ItemIsEditable)
                    widget.setItem(row, col, item)
                else:
                    item.setText(str(criteria_data[row][col]))

        comparison_dict = data["matrix_comparison_alternatives"]
        criteria = data["criteria"]
        for index, cri in enumerate(criteria):
            matrix = comparison_dict[cri]
            widget = self._tab_widget.widget(index + 1)
            rows = widget.rowCount()
            cols = widget.columnCount()
            for row in range(rows):
                for col in range(cols):
                    self._ignore_signal = True
                    item = widget.item(row, col)
                    if not item:
                        item = QTableWidgetItem(str(matrix[row][col]))
                        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled |
                                      Qt.ItemIsEditable)
                        widget.setItem(row, col, item)
                    else:
                        item.setText(str(matrix[row][col]))
