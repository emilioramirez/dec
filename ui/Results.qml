import QtQuick 1.1

Rectangle {
    id: root

    color: "white"

    property int barSize: 400
    property int results: 9

    function set_model(model) {
        for(var i = 0; i < model.length; i++) {
            repeater.model.append(
                {"name": model[i][0],
                 "colorBar": model[i][1],
                 "dataValue": model[i][2]});
        }
        for(var i = 0; i < model.length; i++) {
            repeaterIndex.model.append(
                {"name": model[i][0],
                 "colorBar": model[i][1],
                 "dataValue": model[i][2]});
        }
        root.results = model.length;
    }

    function set_title(title) {
        titleItem.text = title;
    }

    function set_subtitle(subtitle) {
        subtitleItem.text = subtitle;
    }

    Flickable {
        anchors.fill: parent
        anchors.topMargin: 15
        contentHeight: subtitleItem.height + titleItem.height + 20 + row.childrenRect.height + col.childrenRect.height + col.anchors.margins + (col.spacing * root.results) + 50

        Text {
            id: titleItem
            color: "black"
            font.pixelSize: 20
            font.bold: true
            anchors {
                left: parent.left
                right: parent.right
                margins: 10
            }
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            id: subtitleItem
            color: "black"
            font.pixelSize: 16
            anchors {
                left: parent.left
                right: parent.right
                top: titleItem.bottom
                margins: 10
            }
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
        }

        Row {
            id: row
            anchors {
                left: parent.left
                right: parent.right
                top: subtitleItem.bottom
                margins: 10
            }

            spacing: 10

            Rectangle {
                id: indices
                color: "transparent"
                width: 30
                height: root.barSize
                anchors.bottom: parent.bottom

                Text {
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                    horizontalAlignment: Text.AlignHCenter
                    text: "1"
                    color: "black"
                }
                Text {
                    anchors.centerIn: parent
                    text: "0.5"
                    color: "black"
                }
                Text {
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    horizontalAlignment: Text.AlignHCenter
                    text: "0"
                    color: "black"
                }
            }

            Repeater {
                id: repeater
                model: ListModel {}
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                }

                Rectangle {
                    color: colorBar
                    width: ((row.width - indices.width - row.spacing) / root.results) - row.spacing
                    height: root.barSize * dataValue
                    anchors.bottom: parent.bottom

                    Text {
                        anchors.bottom: parent.top
                        anchors.left: parent.left
                        anchors.margins: 5
                        text: dataValue
                        color: "black"
                    }
                }
            }
        }

        Column {
            id: col
            anchors {
                left: parent.left
                right: parent.right
                top: row.bottom
                topMargin: 70
            }
            spacing: 10

            Repeater {
                id: repeaterIndex
                model: ListModel {}
                anchors {
                    left: parent.left
                    right: parent.right
                }

                Rectangle {
                    width: 50
                    height: 50
                    color: colorBar

                    Text {
                        text: name
                        anchors.top: parent.top
                        anchors.left: parent.right
                        anchors.margins: 15
                    }
                }
            }
        }
    }

}
